---
- name: Create
  hosts: localhost
  connection: local
  gather_facts: false
  no_log: "{{ molecule_no_log }}"
  tasks:
    - name: Include read_vars.yml
      include: read_vars.yml

    - name: Pull terraform repo
      git:
        repo: "https://gitlab.com/terraform59/k8s-cluster-on-antsle.git"
        dest: "{{ scratch_dir  }}/k8s-cluster-on-antsle"

    - name: Include antsle_token.yml
      include: antsle_token.yml

    - name: Plan terraform config to Antsle
      community.general.terraform:
        project_path: "{{ scratch_dir }}/k8s-cluster-on-antsle"
        state: planned
        force_init: true
        plan_file: "{{ scratch_dir }}/k8s-cluster-on-antsle/plan.out"
        variables:
          apikey_auth: "Token {{ antsleToken }}"
          template: "{{ antlet_template }}"

    - name: Apply terraform config to Antsle
      community.general.terraform:
        project_path: "{{ scratch_dir }}/k8s-cluster-on-antsle"
        state: present
        plan_file: "{{ scratch_dir }}/k8s-cluster-on-antsle/plan.out"
        variables:
          apikey_auth: "Token {{ antsleToken }}"
          template: "{{ antlet_template }}"

    - name: Include terraform_state.yml
      include: terraform_state.yml

    - name: Print Bridge VNICs API calls
      debug:
        msg: >
          curl -X POST
          --header 'Content-Type: application/json'
          --header 'Accept: application/json'
          --header 'Authorization: Token {{ antsleToken }}'
          -d '{ "model": "virtio", "type": "bridge",
          "source": "{{ antsle_bridge }}" }'
          '{{ antsle_url }}/api/antlets/{{ item }}/vnics'
      changed_when: false
      loop: "{{ tfstatejson | json_query('resources[].name') }}"
      when: debug

    - name: Add Bridge VNICs to Antlets
      command:
        cmd: >
          curl -X POST
          --header 'Content-Type: application/json'
          --header 'Accept: application/json'
          --header 'Authorization: Token {{ antsleToken }}'
          -d '{ "model": "virtio", "type": "bridge",
          "source": "{{ antsle_bridge }}" }'
          '{{ antsle_url }}/api/antlets/{{ item }}/vnics'
        warn: false
      changed_when: false
      loop: "{{ tfstatejson | json_query('resources[].name') }}"
      register: vnics

    # Omit explicit MAC addr specification in API call so that it is calculated
    # and each antlet gets a unqiue MAC.
    # We want a unique MAC per antlet/vnic so each antlet gets a unique IP
    # from DHCP.
    - name: Start Antlets
      command:
        cmd: >
          curl -X POST
          --header 'Content-Type: application/json'
          --header 'Accept: application/json'
          --header 'Authorization: Token {{ antsleToken }}'
          -d '{ "action": "start" }'
          '{{ antsle_url }}/api/antlets/{{ item }}/state'
        warn: false
      changed_when: false
      loop: "{{ tfstatejson | json_query('resources[].name') }}"

    - name: Reboot Antlets (So bridge VNICs get DHCP lease)
      command:
        cmd: >
          curl -X POST
          --header 'Content-Type: application/json'
          --header 'Accept: application/octet-stream'
          --header 'Authorization: Token {{ antsleToken }}'
          '{{ antsle_url }}/api/antlets/{{ item }}/reboot'
        warn: false
      changed_when: false
      loop: "{{ tfstatejson | json_query('resources[].name') }}"

    - name: Create ~/.ssh subdirectory for antsle SSH configs
      file:
        path: ~/.ssh/config.d/
        state: directory

    - name: Set SSH Config include line
      lineinfile:
        path: ~/.ssh/config
        line: "Include config.d/*"
        state: present
        insertbefore: BOF

    - name: Set ~/.ssh/config.d/antsle for Antsle host
      template:
        src: antsle.j2
        dest: ~/.ssh/config.d/antsle

    - name: Include antlets.yml
      include: antlets.yml

    # Generate SSH key
    - name: Generate SSH keypair used for accessing Antlets
      community.crypto.openssh_keypair:
        path: "~/.ssh/{{ antlet_ssh_key_name }}"
        comment: "antlet SSH key"

    # For each pair, render a template file into ~/.ssh/config.d/{{ dname }}
    - name: Set ~/.ssh/config.d/<antlet_name> for Antlet
      template:
        src: antlet.j2
        dest: ~/.ssh/config.d/{{ item.dname }}
      loop: "{{ antletjson }}"
      loop_control:
        label: "{{ item.dname }}"
      vars:
        antlet_name: "{{ item.dname }}"
        antlet_ip: "{{ item.ip }}"

    # For each antlet, run ssh-copy-id to authorize key login to the antlet
    # Invoking ssh-copy-id via Ansible is problematic
    # - name: ssh-copy-id for each Antlet
    #   command:
    #     cmd: >
    #       sshpass -p {{ antlet_template_password }}
    #       ssh-copy-id -i ~/.ssh/{{ antlet_ssh_key_name }}
    #       -o StrictHostKeyChecking=no {{ item.dname }}
    #   loop: "{{ antletjson }}"
    #   loop_control:
    #     pause: 1
    #   ignore_errors: yes

    - name: Run ssh-copy-id manually =(
      pause:
        prompt: >
          Ansible has difficulty running sshpass and ssh-copy-id via command
          and shell modules, so copy and paste the following for yourself
          sshpass -p {{ antlet_template_password }} ssh-copy-id -i ~/.ssh/{{ antlet_ssh_key_name }} -o StrictHostKeyChecking=no k8snode1
          sshpass -p {{ antlet_template_password }} ssh-copy-id -i ~/.ssh/{{ antlet_ssh_key_name }} -o StrictHostKeyChecking=no k8snode2
          sshpass -p {{ antlet_template_password }} ssh-copy-id -i ~/.ssh/{{ antlet_ssh_key_name }} -o StrictHostKeyChecking=no k8snode3
          sshpass -p {{ antlet_template_password }} ssh-copy-id -i ~/.ssh/{{ antlet_ssh_key_name }} -o StrictHostKeyChecking=no k8snode4

    - name: Include antlet_vnics.yml
      include: antlet_vnics.yml

    - name: Remove ProxyJump line for each Antlet
      lineinfile:
        path: ~/.ssh/config.d/{{ item.dname }}
        regexp: 'ProxyJump {{ antsle_hostname }}'
        state: absent
      loop: "{{ antletjson }}"
      loop_control:
        label: "{{ item.dname }}"

    - name: Update IP address for each Antlet to VNIC IP
      replace:
        path: ~/.ssh/config.d/{{ item.key }}
        regexp: 'Hostname (?:[0-9]{1,3}\.){3}[0-9]{1,3}$'
        replace: "Hostname {{ item.value }}"
      with_dict: "{{ vnics }}"

    # Set molecule instance_config.yml file in cache directory

    - when: server.changed | default(true) | bool
      block:
        - name: Populate instance config dict
          set_fact:
            instance_conf_dict: {
              'instance': "{{ item.key }}",
              'address': "{{ item.value }}",
              'user': "{{ antlet_template_user }}",
              'port': "22",
              'identity_file': "~/.ssh/{{ antlet_ssh_key_name }}", }
          with_dict: "{{ vnics }}"
          register: instance_config_dict

        - name: Convert instance config dict to a list
          set_fact:
            instance_conf: >
              {{ instance_config_dict.results |
              map(attribute='ansible_facts.instance_conf_dict') |
              list }}

        - name: Dump instance config
          copy:
            content: |
              # Molecule managed

              {{ instance_conf | to_json | from_json | to_yaml }}
            dest: "{{ molecule_instance_config }}"
