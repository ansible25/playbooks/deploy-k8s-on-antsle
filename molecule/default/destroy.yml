---
- name: Destroy
  hosts: localhost
  connection: local
  gather_facts: false
  no_log: "{{ molecule_no_log }}"
  tasks:
    - name: Include read_vars.yml
      include: read_vars.yml

    - name: Refresh facts
      block:
        - name: Include antsle_token.yml
          include: antsle_token.yml

        - name: Include terraform_state.yml
          include: terraform_state.yml

        - name: Include antlets.yml
          include: antlets.yml

    - name: Print Antlet Shutdown API calls
      debug:
        msg: >
          curl -X POST
          --header 'Content-Type: application/json'
          --header 'Accept: application/json'
          --header 'Authorization: Token {{ antsleToken }}'
          -d '{ "action": "stop" }' '{{ antsle_url }}/api/antlets/{{ item }}/state'"
      changed_when: false
      loop: "{{ tfstatejson | json_query('resources[].name') }}"
      when: debug

    - name: Stop Antlets
      command:
        cmd: "curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Token {{ antsleToken }}' -d '{ \"action\": \"stop\" }' '{{ antsle_url }}/api/antlets/{{ item }}/state'"
        warn: false
      changed_when: false
      loop: "{{ tfstatejson | json_query('resources[].name') }}"

    - name: Pause for 30 seconds to allow antlets to finish stopping
      pause:
        seconds: 30

    - name: Destroy terraform config on Antsle
      community.general.terraform:
        project_path: "{{ scratch_dir }}/k8s-cluster-on-antsle"
        state: absent
        variables:
          apikey_auth: "Token {{ antsleToken }}"
          template: "{{ antlet_template }}"
      retries: 3
      delay: 3
      register: result
      until: result is succeeded

    - name: Remove git repo
      file:
        path: "{{ scratch_dir }}/k8s-cluster-on-antsle"
        state: absent

    - name: Remove ansible facts cache
      file:
        path: "{{ scratch_dir }}/facts_cache/localhost"
        state: absent

    - name: Remove SSH config include line
      lineinfile:
        path: ~/.ssh/config
        line: "Include config.d/*"
        state: absent

    - name: Remove added ~/.ssh/config.d/antsle
      file:
        path: ~/.ssh/config.d/antsle
        state: absent

    - name: Remove ~/.ssh/config.d/<antlet_name> for Antlet
      file:
        path: ~/.ssh/config.d/{{ item.dname }}
        state: absent
      loop: "{{ antletjson }}"
      vars:
        antlet_name: "{{ item.dname }}"
        antlet_ip: "{{ item.ip }}"

    # - name: Remove generated SSH private key
    #   file:
    #     path: "~/.ssh/{{ antlet_ssh_key_name }}"
    #     state: absent

    # - name: Remove generated SSH public key
    #   file:
    #     path: "~/.ssh/{{ antlet_ssh_key_name }}.pub"
    #     state: absent

    - name: Remove Kubespray virtualenv (virtualenvwrapper)
      shell:
        cmd: >
          . {{ virtualenvwrapper_script }} &&
          rmvirtualenv  {{ virtualenv_name }}
        executable: /bin/bash

    - name: Remove Kubespray checkpoint
      file:
        path: /tmp/kubespray
        state: absent

    - name: Remove KUBECONFIG file
      file:
        path: ~/.kube/config
        state: absent

    # Mandatory configuration for Molecule to function.

    - name: Populate instance config
      set_fact:
        instance_conf: {}

    - name: Dump instance config
      copy:
        content: |
          # Molecule managed

          {{ instance_conf | to_json | from_json | to_yaml }}
        dest: "{{ molecule_instance_config }}"
      when: server.changed | default(false) | bool
