# Table of Contents
* [Introduction](#introduction)
  * [Quick Start](#quick-start)
  * [How it Works](#how-it-works)
* [Setup](#setup)
* [License](#license)
* [Author](#author)

---

# Introduction

This project (ab)uses the [Ansible Molecule](https://molecule.readthedocs.io/en/latest/) testing framework to boil down the process of deploying a kubernetes cluster onto an [Anstle device](https://antsle.com/) into a single command*: `molecule converge`

\* Disclaimer: Ansible has some trouble running the needed `ssh-copy-id` commands on your behalf, so one has to copy and paste the generated commands in a separate terminal (a dedicated TTY) to get the commands to work.

## Quick Start
1. git clone this repo
2. Install Ansible on your system and configure SSH access on your system
3. `ansible-galaxy install -r ./requirements.yml`
4. Install Molecule on your system
5. Install Terraform on your system
6. Add your variables to `molecule/default/vars_sensitive_example.yml` and rename the file to `vars_sensitive.yml`
7. Run `molecule converge` from within the project directory

## How it Works
In a nutshell, Molecule is serving as the automation driver orchestrating the various tasks needed to construct a kubernetes cluster from scratch.

![How it Works](assets/molecule_antsle_driver.jpg "How it Works")


The Molecule framework provides the concept of a [delegated driver](https://molecule.readthedocs.io/en/latest/configuration.html?highlight=delegated#delegated). One's localhost is used for the delegated driver; this means your system is serving as the "target" of Molecule actions. It is also serving as an Ansible Controller and Terraform controller. `molecule create` targets your localhost, applies Terraform from [k8s-cluster-on-antsle](https://gitlab.com/terraform59/k8s-cluster-on-antsle) against your designated Antsle device to create 4 Antlets, then calls Antsle APIs to initailize them and registers them into Molecule's inventory. The Antlets are prepared by the `prepare.yml` playbook to update them and prepare them for convergence.

`molecule converge` git clones, configures, and runs [Kubespray](https://github.com/kubernetes-sigs/kubespray/) against your Antlets to render them into a working kubernetes cluster.

# Setup
## Install Ansible on Your System and Configure the Terraform OpenAPI Provider
Install Ansible however you'd like; from source, your package manager, or pip. Kubespray's Ansible version capped at version 2.11, so it is installed into a Python virtualenv to prevent collision with the system's version of Ansible. Ansible 2.10.4 was used for developing and running this repo's content, but it also works with version 2.12.1.

[This blog post discusses how to setup the OpenAPI provider for your Antsle](https://bradthebuilder.me/blog/2021/antsle-and-terraform.html)


## Install Molecule on Your System
`python3 -m pip install -U molecule`

## Install Terraform on Your System
1. [Download from Hashicorp's website](https://www.terraform.io/downloads.html)
2. Put on your $PATH
3. Grant binary execute permissions

Terraform 1.0.1 was used for development of this project, but version 1.1.7 works as well.

## Vars
* scratch_dir: The directory where Kubespray and [k8s-cluster-on-antsle](https://gitlab.com/terraform59/k8s-cluster-on-antsle) are Git cloned to and the Ansible Facts cache is written to
* antsle_url: The URL of your Antsle
* antsle_ssh_port: The SSH port Ansible should use to connect to your Antsle
* antsle_user_ssh_key_name: The name of the private SSH key Ansible will use to connect to your Antsle
* antsle_bridge: The bridge name/ID to map the created Antlets to
* antlet_ssh_key_name: The name of the private SSH key Ansible will use to connect to your Antlets
* antlet_template: The name of the [Antlet template](https://docs.antsle.com/templates/templates) used to create your Antlets. **Your template must support DHCP for the created bridge NIC without Ansible intervention** The bridged NIC is created *so that* Ansible can manage the Antlets directly. Some Antsle-provided templates, like the Debian 11 template, do not support this out of the box, so you may have to create your own Antlet template to satisfy this requirement. The default value for this variable emphasises this.
* virtualenvwrapper_script: The absolute path to the [virtualenvwrapper script](https://virtualenvwrapper.readthedocs.io/en/latest/index.html) on your system
* virtualenv_name: The arbitrary name of the virtualenv used to install Kubespray's requirements/dependencies into
* debug: A boolean value determing whether or not to print extra debugging information during Ansible run


## Sensitive Vars
* antsle_hostname: The DNS resolvable hostname of your Antsle
* antsle_username: The username used to log into your Antsle (SSH/API)
* antsle_password: The password used for antsle_username
* antlet_template_user: [The username used for Antlet template](https://docs.antsle.com/templates/templates)
* antlet_template_password: [The password used for Antlet template](https://docs.antsle.com/templates/templates)

# License

[license](LICENSE) (MIT)

# Author

[Brad Stinson](https://www.bradthebuilder.me/)

[Back to table of contents](#table-of-contents)
